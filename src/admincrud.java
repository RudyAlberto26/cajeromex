import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;



public class admincrud {
	private ObjectContainer db;
	
	private void abrirConexion() {
		db=Db4oEmbedded.openFile("Cajerodb");
	}
	
	private void cerrarConexion() {
		db.close();
	}

void insertarRegistro(clientes Clientes) {
	abrirConexion();
	db.store(Clientes);
	
	cerrarConexion();
}
public clientes seleccionarUsuario(clientes Clientes ) {
	abrirConexion();
	ObjectSet resultado=db.queryByExample(Clientes);
	clientes usuario=(clientes) resultado.next();
	cerrarConexion();
	return usuario;
}

//Metodo de Update

	public void actualizarRegistro(int id,String nombre,Double fondoInicial,int numTarjeta, int nip) {

		abrirConexion();
		clientes p = new clientes(); 
		p.setNumeroTarjeta(numTarjeta);
		ObjectSet resultado=db.queryByExample(p);
		clientes auxiliar=(clientes) resultado.next();
		auxiliar.setIdUsuario(id);
		auxiliar.setNombreCliente(nombre);
		auxiliar.setSaldo(fondoInicial);
		auxiliar.setNumeroTarjeta(numTarjeta);
		auxiliar.setNip(nip);
		db.store(auxiliar);
		cerrarConexion();
	}
}
