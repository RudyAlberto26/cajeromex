import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class main {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main window = new main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.inactiveCaption);
		frame.getContentPane().setForeground(Color.BLACK);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Que desea realizar:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(122, 79, 159, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnAdmin = new JButton("Usuario Administrador");
		btnAdmin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			CrearUsuario crear = new CrearUsuario();
			crear.setVisible(true);
				
			}
		});
		btnAdmin.setBounds(115, 104, 166, 23);
		frame.getContentPane().add(btnAdmin);
		
		JButton btnCajero = new JButton("Entrar al Cajero");
		btnCajero.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				PrincipalCajero crear = new PrincipalCajero();
				crear.setVisible(true);
				
			}
		});
		btnCajero.setBounds(115, 151, 171, 23);
		frame.getContentPane().add(btnCajero);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(115, 203, 166, 23);
		frame.getContentPane().add(btnSalir);
		
		JLabel lblNewLabel_1 = new JLabel("Bienvenido a CajeroMex");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(109, 11, 199, 51);
		frame.getContentPane().add(lblNewLabel_1);
	}
}
