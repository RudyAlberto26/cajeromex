

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;

public class PrincipalCajero extends JFrame {

	private JPanel contentPane;
	private JTextField txtNumTarjeta1;
	private JTextField txtNip1;
	JLabel lblNewLabel,lblNewLabel_1;
	JButton btnEntrar;
	private JLabel lbNombre;
	private JLabel lbFondos;
	private static JButton btnDepositar;
	private static JButton btnRetirar;
	private static JButton btnSalir;
	private static JLabel lblNewLabel_3;
	private static JTextField txtEfectivo;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrincipalCajero frame = new PrincipalCajero();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrincipalCajero() {
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.inactiveCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
	    lblNewLabel = new JLabel("Num. Tarjeta:");
		lblNewLabel.setBounds(14, 83, 91, 14);
		contentPane.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Nip:");
		lblNewLabel_1.setBounds(14, 129, 73, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Bienvenido al Cajeromex");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNewLabel_2.setBounds(83, 11, 274, 79);
		contentPane.add(lblNewLabel_2);
		
		txtNumTarjeta1 = new JTextField();
		txtNumTarjeta1.addMouseListener(new MouseAdapter() {
			
		});
		txtNumTarjeta1.setBounds(115, 80, 242, 20);
		contentPane.add(txtNumTarjeta1);
		txtNumTarjeta1.setColumns(10);
		
		txtNip1 = new JTextField();
		txtNip1.setBounds(115, 126, 104, 20);
		contentPane.add(txtNip1);
		txtNip1.setColumns(10);
		
		 btnEntrar = new JButton("Entrar");
		 btnEntrar.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 	}
		 });
		btnEntrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				 lbNombre.setVisible(false);
				 lbFondos.setVisible(false);

			  admincrud metodos = new admincrud();
				clientes usuario=new clientes();
				
				usuario.setNumeroTarjeta(Integer.parseInt(txtNumTarjeta1.getText()));
				usuario = metodos.seleccionarUsuario(usuario);
				int tarjeta=(Integer.parseInt(txtNumTarjeta1.getText()));
				int tarjeta1=usuario.getNumeroTarjeta();
				int pin=(Integer.parseInt(txtNip1.getText()));
				int pin1=usuario.getNip();
				Double fondos= usuario.getSaldo();
				if(tarjeta == tarjeta1 && pin == pin1 ){
					JOptionPane.showMessageDialog(null, "Bienvenido: "+ usuario.getNombreCliente(), " ", JOptionPane.INFORMATION_MESSAGE);
					lbNombre.setText(usuario.getNombreCliente());
					lbFondos.setText("$"+fondos);
					Menuinicio();
				}else {
					
					JOptionPane.showMessageDialog(null, "El Nip de la tarjeta: "+ tarjeta1 + " \n Es incorrecto", getTitle(), JOptionPane.WARNING_MESSAGE);
					
					
				}
					
			
			}
			
			
		});
		btnEntrar.setBounds(234, 125, 89, 23);
		contentPane.add(btnEntrar);
		
		lbNombre = new JLabel("New label");
		lbNombre.setBounds(115, 83, 46, 14);
		contentPane.add(lbNombre);
		
		lbFondos = new JLabel("New label");
		lbFondos.setBounds(115, 126, 46, 14);
		contentPane.add(lbFondos);
		
		btnDepositar = new JButton("Depositar");
		btnDepositar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				admincrud metodos = new admincrud();
				clientes usuarios=new clientes();
				
				usuarios.setNumeroTarjeta(Integer.parseInt(txtNumTarjeta1.getText()));
				usuarios= metodos.seleccionarUsuario(usuarios);
				
				
				
				int id= (usuarios.getIdUsuario());
				String nom=(usuarios.getNombreCliente());
				Double fondF=Double.parseDouble(txtEfectivo.getText());
				Double fond1=(usuarios.getSaldo());
				int numTarjeta=(usuarios.getNumeroTarjeta());
				int NIP=(usuarios.getNip());
				Double fondI=fondF+fond1;
				
				metodos.actualizarRegistro(id, nom, fondI, numTarjeta, NIP);
				
				JOptionPane.showMessageDialog(null, "La cantidad depositada son: "+ fondF, getTitle(), JOptionPane.WARNING_MESSAGE);
				String  fondos=usuarios.getSaldo().toString();
				lbFondos.setText(fondos);
				
				
			}
		});
		btnDepositar.setVisible(false);
		btnDepositar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDepositar.setBounds(112, 202, 89, 23);
		contentPane.add(btnDepositar);
		
		btnRetirar = new JButton("Retirar");
		btnRetirar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				admincrud metodos = new admincrud();
				clientes usuarios=new clientes();
				usuarios.setNumeroTarjeta(Integer.parseInt(txtNumTarjeta1.getText()));
				usuarios= metodos.seleccionarUsuario(usuarios);
				
				
				
				int id= (usuarios.getIdUsuario());
				String nombre=(usuarios.getNombreCliente());
				Double fondosIntroducidos=Double.parseDouble(txtEfectivo.getText());
				Double fondoInicial=(usuarios.getSaldo());
				int numTarjeta=(usuarios.getNumeroTarjeta());
				int NIP=(usuarios.getNip());
				
				if(fondosIntroducidos<=fondoInicial) {
					Double fondTotal=fondoInicial-fondosIntroducidos;
					
					metodos.actualizarRegistro(id, nombre, fondTotal, numTarjeta, NIP);

					JOptionPane.showMessageDialog(null, "La cantidad a retirar es: "+ fondosIntroducidos, getTitle(), JOptionPane.WARNING_MESSAGE);
			

					String  fondos=usuarios.getSaldo().toString();
					lbFondos.setText(fondos);
					
				}else {
				
				JOptionPane.showMessageDialog(null, "No cuentas con los fondos necesario para esa transaccion", "Fondos Insuficientes", JOptionPane.WARNING_MESSAGE);

				String  fondos=usuarios.getSaldo().toString();
				lbFondos.setText(fondos);
				}
				
			 		
			 		
			 		

				
			}
		});
		btnRetirar.setVisible(false);
		btnRetirar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnRetirar.setBounds(211, 202, 89, 23);
		contentPane.add(btnRetirar);
		
		btnSalir = new JButton("Salir");
		btnSalir.setVisible(false);
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				System.exit(0);
				
				
			}
		});
		btnSalir.setBounds(312, 202, 89, 23);
		contentPane.add(btnSalir);
		
		lblNewLabel_3 = new JLabel("Ingrese una Cantidad: ");
		lblNewLabel_3.setVisible(false);
		lblNewLabel_3.setBounds(32, 172, 129, 14);
		contentPane.add(lblNewLabel_3);
		
		txtEfectivo = new JTextField();
		txtEfectivo.setVisible(false);
		
		txtEfectivo.setBounds(161, 169, 86, 20);
		contentPane.add(txtEfectivo);
		txtEfectivo.setColumns(10);
		
		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				PrincipalCajero o = new PrincipalCajero();
				o.setVisible(true);
				
			}
		});
		btnActualizar.setBounds(16, 202, 89, 23);
		contentPane.add(btnActualizar);
	}
	
	public void Menuinicio() {
		txtNumTarjeta1.setVisible(false);
		txtNip1.setVisible(false);
		btnEntrar.setVisible(false);
	//	btnregresar.setVisible(false);
		
		btnSalir.setVisible(true);
		btnDepositar.setVisible(true);
		btnRetirar.setVisible(true);
		
		lblNewLabel_3.setVisible(true); 
		txtEfectivo.setVisible(true);
		
		lblNewLabel.setText("Nombre: ");
		lblNewLabel_1.setText("Fondos: ");
		lbNombre.setVisible(true);
		lbFondos.setVisible(true);
	 
		
	}
}
