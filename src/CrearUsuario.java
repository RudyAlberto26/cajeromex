import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CrearUsuario extends JFrame {

	private JFrame frame;
	private JTextField textId;
	private JTextField textNombre;
	private JTextField textApellidoPat;
	private JTextField textNumeroTar;
	private JTextField textNip;
	private JTextField textSaldo;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrearUsuario window = new CrearUsuario();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CrearUsuario() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.activeCaption);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Crear usuario");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 24));
		lblNewLabel.setBounds(58, 11, 200, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Id:");
		lblNewLabel_1.setBounds(58, 49, 77, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Nombre:");
		lblNewLabel_2.setBounds(58, 85, 66, 14);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Apellido Paterno:");
		lblNewLabel_3.setBounds(58, 122, 97, 14);
		frame.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Numero de tarjeta:");
		lblNewLabel_4.setBounds(58, 147, 123, 14);
		frame.getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Nip:");
		lblNewLabel_5.setBounds(61, 172, 63, 14);
		frame.getContentPane().add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("Saldo Inicial:");
		lblNewLabel_6.setBounds(58, 197, 97, 14);
		frame.getContentPane().add(lblNewLabel_6);
		
		textId = new JTextField();
		textId.setBounds(209, 46, 123, 20);
		frame.getContentPane().add(textId);
		textId.setColumns(10);
		
		textNombre = new JTextField();
		textNombre.setBounds(209, 82, 123, 20);
		frame.getContentPane().add(textNombre);
		textNombre.setColumns(10);
		
		textApellidoPat = new JTextField();
		textApellidoPat.setBounds(209, 119, 123, 20);
		frame.getContentPane().add(textApellidoPat);
		textApellidoPat.setColumns(10);
		
		textNumeroTar = new JTextField();
		textNumeroTar.setBounds(209, 144, 123, 20);
		frame.getContentPane().add(textNumeroTar);
		textNumeroTar.setColumns(10);
		
		textNip = new JTextField();
		textNip.setBounds(209, 169, 123, 20);
		frame.getContentPane().add(textNip);
		textNip.setColumns(10);
		
		textSaldo = new JTextField();
		textSaldo.setBounds(209, 194, 123, 20);
		frame.getContentPane().add(textSaldo);
		textSaldo.setColumns(10);
		
		JButton btnGuardar = new JButton("Guardar Usuario");
		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				clientes Client = new clientes();
				admincrud Funcion=new  admincrud();
				
				Client.setIdUsuario(Integer.parseInt(textId.getText()));
				Client.setNombreCliente(textNombre.getText());
				Client.setApellidoPat(textApellidoPat.getText());
				Client.setNumeroTarjeta(Integer.parseInt(textNumeroTar.getText()));
				//Client.setNumeroTarjeta(Integer.parseInt(textNumeroTar.getText()));
				Client.setNip(Integer.parseInt(textNip.getText()));
				Client.setSaldo(Double.parseDouble(textSaldo.getText()));
				
				
				Funcion.insertarRegistro(Client);
				
				System.out.println ("Insercion Correctamente");
			}
		});
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnGuardar.setBounds(91, 227, 135, 23);
		frame.getContentPane().add(btnGuardar);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(280, 227, 89, 23);
		frame.getContentPane().add(btnSalir);
	}
}
