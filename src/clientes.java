
public class clientes {
	private int idUsuario;
	private String NombreCliente;
	private String ApellidoPat;
	private int NumeroTarjeta;
	private int Nip;
	private Double Saldo;
	
	
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombreCliente() {
		return NombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		NombreCliente = nombreCliente;
	}
	public String getApellidoPat() {
		return ApellidoPat;
	}
	public void setApellidoPat(String apellidoPat) {
		ApellidoPat = apellidoPat;
	}
	public int getNumeroTarjeta() {
		return NumeroTarjeta;
	}
	public void setNumeroTarjeta(int numeroTarjeta) {
		NumeroTarjeta = numeroTarjeta;
	}
	public int getNip() {
		return Nip;
	}
	public void setNip(int nip) {
		Nip = nip;
	}
	public Double getSaldo() {
		return Saldo;
	}
	public void setSaldo(Double saldo) {
		Saldo = saldo;
	}
	
	@Override
	public String toString() {
		return "clientes"
				+ ": \n" + "idUsuario=" + idUsuario + ",\n NombreCliente=" + NombreCliente + ", \n ApellidoPat=" + ApellidoPat
				+ ", \n NumeroTarjeta=" + NumeroTarjeta + ", \n Nip=" + Nip + ", \n Saldo" + Saldo;
	}
	
}
