

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.SystemColor;

public class ConsultarUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField txtID;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultarUsuario frame = new ConsultarUsuario();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ConsultarUsuario() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Consultar");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				admincrud metodos=new  admincrud();
				clientes usuario=new clientes();
				usuario.setIdUsuario(Integer.parseInt(txtID.getText()));
				usuario = metodos.seleccionarUsuario(usuario);
				
JOptionPane.showMessageDialog(null, "Los datos seleccionados son: \n "+ usuario, getTitle(), JOptionPane.WARNING_MESSAGE);

			
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(207, 68, 89, 23);
		contentPane.add(btnNewButton);
		
		txtID = new JTextField();
		txtID.setBounds(111, 69, 86, 20);
		contentPane.add(txtID);
		txtID.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("ID usuario:");
		lblNewLabel.setBounds(10, 72, 74, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnregresar = new JButton("Atras");
		btnregresar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			
				//PrincipalAdmin principal = new PrincipalAdmin();
				//principal.setVisible(true);
				
			}
		});
		btnregresar.setBounds(306, 68, 89, 23);
		contentPane.add(btnregresar);
	}
}
